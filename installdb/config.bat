
IF EXIST "%~dp0tools\loadenv.bat" CALL "%~dp0tools\loadenv.bat"

:: 服务器。
REM IF "%HOST%"==""              SET HOST=121.196.13.252
IF "%HOST%"==""              SET HOST=localhost
:: 端口号。
IF "%PORT%"==""              SET PORT=3306
:: 用户名。
IF "%USER%"==""              SET USER=root
:: 登录密码。
IF "%PASSWORD%"==""          SET PASSWORD=123456
:: 数据库名称。
IF "%DATABASE%"==""          SET DATABASE=renren_security

:: 应用程序用户名称。
IF "%APPUSER%"==""           SET APPUSER=renren
:: 应用程序用户密码。
IF "%APPPASSWORD%"==""       SET APPPASSWORD=123456
:: 是否允许应用程序远程访问：0：不允许（默认），1：允许。
IF "%APPREMOTEACCESS%"==""   SET APPREMOTEACCESS=0

SET RUNSCRIPT=mysql --host=%HOST% --user=%USER% --password=%PASSWORD% --port=%PORT% --comments

