
:: #############################################
:: # MYSQL运维工具 - 数据升级
:: # AUTHORS       : 郭恒辉
:: # LAST MODIFY   : 2020-08-04
:: #############################################

@ECHO OFF
@ECHO ########################################################
@ECHO 【开始升级】
SETLOCAL
CALL "%~dp0config.bat"

mysql --host=%HOST% --user=%USER% --password=%PASSWORD% --port=%PORT% --comments <"install-upgrades.sql"

@ECHO ********************************
@ECHO *                              *
@ECHO *                              *
IF "%ERRORLEVEL%" == "0" (
@ECHO *          升 级 成 功
)
IF NOT "%ERRORLEVEL%" == "0" (
@ECHO *  升级失败！错误码：%ERRORLEVEL%
@ECHO *                              *
@ECHO *  请检查配置：config.bat
)
@ECHO *                              *
@ECHO *                              *
@ECHO ********************************
@ECHO ########################################################	
PAUSE