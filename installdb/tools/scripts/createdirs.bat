
@IF "%~1" == "" (
  @GOTO :_ShowHelp
)

@IF /I "%~1" == "-h" ( GOTO :_ShowHelp )
@IF /I "%~1" == "--help" ( GOTO :_ShowHelp )
@IF /I "%~1" == "/?" ( GOTO :_ShowHelp )

@GOTO :CreateDir

:: ################################################
:: Functions
:_ShowHelp
	@ECHO Usage:
	@ECHO    %~n0 File1 File2 ...
	@ECHO Arguments:
	@ECHO    -h --help /?    : Show help

@EXIT /B 0

:CreateDir
    @SETLOCAL
	:CreateDirLoop
		@IF "%~1" == "" ENDLOCAL & EXIT /B 0 
		@IF NOT EXIST "%~1" (
			@MKDIR "%~1"
			@IF NOT EXIST "%~1" (
				@ENDLOCAL & EXIT /B 1
			)
		)
	@SHIFT
	@GOTO :CreateDirLoop
@ENDLOCAL & EXIT /B 0