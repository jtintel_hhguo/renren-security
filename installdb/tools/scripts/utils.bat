@IF "%~1" == "" (
  @GOTO :_ShowHelp
)

@IF "%~1" == "-h" ( GOTO :_ShowHelp )
@IF "%~1" == "--help" ( GOTO :_ShowHelp )
@IF "%~1" == "/?" ( GOTO :_ShowHelp )

@SET _FUNCTION_=%~1
@IF NOT "%_FUNCTION_:~0,1%"==":" SET _FUNCTION_=:%_FUNCTION_%

@CALL :_ClearError
@GOTO %_FUNCTION_%

:_ShowHelp
	@ECHO Usage:
	@ECHO    %~n0 Function [Arg1] [Arg2] [Arg3] ...
	@ECHO Arguments:
	@ECHO    -h --help /?    : Show help
	@ECHO ----------------------------------  
	@ECHO    Fuctions        : Arguments
	@ECHO ----------------------------------  
	@ECHO    Date            : return [Format]
	@ECHO                      Format: 
	@ECHO                        0[Default]: yyyyMMddHHmmss.ffffff+nnn 
	@ECHO                        1: yyyy-MM-dd   2: yyyyMMdd
	@ECHO                        3: HH:mm:ss.fff 4: HHmmssfff
	@ECHO                        5: HH:mm:ss     6: HHmmss
	@ECHO                        7: yyyy-MM-ddTHH:mm:ss.fff
	@ECHO                        8: yyyy-MM-dd HH:mm:ss.fff
	@ECHO                        9: yyyy-MM-dd HH:mm:ss
	@ECHO                        A: yyyyMMdd-HHmmssfff
	@ECHO                        B: yyyyMMdd-HHmmss
	@ECHO                        C: yyyyMMddHHmmssfff
	@ECHO                        D: yyyyMMddHHmmss
	@ECHO    Download        : remoteFile dst 0[1/2/Empty]
	@ECHO                        0/Empty[Default]: dst is a file;
	@ECHO                        1: dst is a directory, do not create;
	@ECHO                        2: dst is a directory, create if not exist;
	@ECHO                      Examples: 
	@ECHO                        http://host/a.zip c:\file1.zip
	@ECHO                        http://host/a.zip c:\dir1 1
	@ECHO    Zip             : sourceFile/sourceDirectory packageFile
	@ECHO    Unzip           : packageFile targetDirectory
	@ECHO    GetVersion      : return fileName
	@ECHO    NewFileName     : return baseFileName extName [Format]
	@ECHO                      Format: 
	@ECHO                        0[Default]: [baseFileName]-N:2~-[extName]
	@ECHO                        1: [baseFileName]-(N:2~)-[extName]

@EXIT /B 0
:: ################################################

:_ClearError
	VER >NUL
	GOTO :EOF
:_ClearErrorEND

:: Get local date time
:: %1: return
:Date
:GetLocalDateTime
	SHIFT
	IF "%~1" == "" GOTO : GetLocalDateTimeEnd
	
	SETLOCAL
	FOR /F "skip=1" %%D IN ('WMIC OS GET LocalDateTime') DO (SET "LIDATE=%%D" & GOTO :GETLOCALDATETIME_GOT_LIDATE)
	:GETLOCALDATETIME_GOT_LIDATE

	SET T=%LIDATE%
	IF NOT "%~2" == "" SET FMT=%~2
	
	IF "%FMT%" == "" ( GOTO :GetLocalDateTimeEnd )
	IF /I "%FMT:~0,1%" GTR == "D" ( GOTO :GetLocalDateTimeEnd )
	IF /I "%FMT:~0,1%" GTR == "9" ( GOTO :GetLocalDateTimeEnd_A )
		IF "%FMT%" == "1" ( SET "LIDATE=%T:~0,4%-%T:~4,2%-%T:~6,2%" & GOTO :GetLocalDateTimeEnd )
		IF "%FMT%" == "2" ( SET "LIDATE=%T:~0,4%%T:~4,2%%T:~6,2%" & GOTO :GetLocalDateTimeEnd )
		IF "%FMT%" == "3" ( SET "LIDATE=%T:~8,2%:%T:~10,2%:%T:~12,2%.%T:~15,3%" & GOTO :GetLocalDateTimeEnd )
		IF "%FMT%" == "4" ( SET "LIDATE=%T:~8,2%%T:~10,2%%T:~12,2%%T:~15,3%" & GOTO :GetLocalDateTimeEnd )
		IF "%FMT%" == "5" ( SET "LIDATE=%T:~8,2%:%T:~10,2%:%T:~12,2%" & GOTO :GetLocalDateTimeEnd )
		IF "%FMT%" == "6" ( SET "LIDATE=%T:~8,2%%T:~10,2%%T:~12,2%" & GOTO :GetLocalDateTimeEnd )
		IF "%FMT%" == "7" ( SET "LIDATE=%T:~0,4%-%T:~4,2%-%T:~6,2%T%T:~8,2%:%T:~10,2%:%T:~12,2%.%T:~15,3%" & GOTO :GetLocalDateTimeEnd )
		IF "%FMT%" == "8" ( SET "LIDATE=%T:~0,4%-%T:~4,2%-%T:~6,2% %T:~8,2%:%T:~10,2%:%T:~12,2%.%T:~15,3%" & GOTO :GetLocalDateTimeEnd )
		IF "%FMT%" == "9" ( SET "LIDATE=%T:~0,4%-%T:~4,2%-%T:~6,2% %T:~8,2%:%T:~10,2%:%T:~12,2%" & GOTO :GetLocalDateTimeEnd )
	:GetLocalDateTimeEnd_A
		IF /I "%FMT%" == "A" ( SET "LIDATE=%T:~0,4%%T:~4,2%%T:~6,2%-%T:~8,2%%T:~10,2%%T:~12,2%%T:~15,3%" & GOTO :GetLocalDateTimeEnd )
		IF /I "%FMT%" == "B" ( SET "LIDATE=%T:~0,4%%T:~4,2%%T:~6,2%-%T:~8,2%%T:~10,2%%T:~12,2%" & GOTO :GetLocalDateTimeEnd )
		IF /I "%FMT%" == "C" ( SET "LIDATE=%T:~0,4%%T:~4,2%%T:~6,2%%T:~8,2%%T:~10,2%%T:~12,2%%T:~15,3%" & GOTO :GetLocalDateTimeEnd )
		IF /I "%FMT%" == "D" ( SET "LIDATE=%T:~0,4%%T:~4,2%%T:~6,2%%T:~8,2%%T:~10,2%%T:~12,2%" & GOTO :GetLocalDateTimeEnd )

:GetLocalDateTimeEnd
ENDLOCAL & SET "%~1=%LIDATE%" & EXIT /B %ERRORLEVEL%

:Download
	SHIFT
	SETLOCAL
	
	IF NOT "%~1" == "" SET remoteFile=%~1
	IF NOT "%~2" == "" SET localFile=%~2
	IF NOT "%~3" == "" SET dstIsDir=%~3

	IF NOT "%localFile%"=="" GOTO :Download_GetFileNameEnd
	:Download_GetFileName
		FOR %%I IN ("%remoteFile%") DO (
			SET localFile=%%~nxI
		)
	:Download_GetFileNameEnd
	
	IF NOT "%dstIsDir%" == "1" GOTO :Download_GetFileNameBySrcEnd
	IF NOT EXIST "%localFile%" MKDIR "%localFile%"
	REM IF NOT EXIST "%localFile%\." ENDLOCAL & EXIT /B 1
	:Download_GetFileNameBySrc
		FOR %%I IN ("%remoteFile%") DO (
			SET srcFileName=%%~nxI
		)
		SET localFile=%localFile%\%srcFileName%
	:Download_GetFileNameBySrcEnd

	IF EXIST "%localFile%" GOTO :DownloadEnd

	SET tmpLocalFile=%localFile%.tmp
	curl -o "%tmpLocalFile%" "%remoteFile%"
	IF %ERRORLEVEL%==0 (
		MOVE /Y "%tmpLocalFile%" "%localFile%"
	)

	IF EXIST "%tmpLocalFile%" DEL "%tmpLocalFile%"
:DownloadEnd
ENDLOCAL & EXIT /B %ERRORLEVEL%

:Zip
	SHIFT
	SETLOCAL
	SET pkg=%~2
	IF EXIST "%pkg%" DEL "%pkg%"
	
	SET PWDARG=
	IF NOT "%ZIPPASSWORD%" == "" SET PWDARG="-p%ZIPPASSWORD%"
	
	IF /I "%pkg:~-3,3%"==".7z" GOTO :Zip_7z
	:: GOTO :Zip_Zip
	GOTO :Zip_7z

	:Zip_7z
		IF EXIST "%~1\" (
			7z a "%pkg%" "%~1" -mx=9 %PWDARG%
			GOTO :ZipEnd
		)
		7z a "%pkg%" "%~1" -mx=9 %PWDARG%
		GOTO :ZipEnd
	:Zip_Zip
		zip -q -r -D "%pkg%" "%~1"
		GOTO :ZipEnd
:ZipEnd
ENDLOCAL & EXIT /B %ERRORLEVEL%

:Unzip
	SHIFT
	SETLOCAL
	
	SET PWDARG=
	IF NOT "%ZIPPASSWORD%" == "" SET PWDARG="-p%ZIPPASSWORD%"
	
	SET pkg=%~1
	:: IF /I "%pkg:~-3,3%"==".7z" (
		7z x -aoa "%pkg%" "-o%~2" %PWDARG%
		GOTO :UnzipEnd
	:: )
	
	:: unzip -o -d "%~2" "%pkg%"
:UnzipEnd
ENDLOCAL & EXIT /B %ERRORLEVEL%

:GetVersion
:ReadVer
	SHIFT
	SETLOCAL
	IF "%~1" == "" ( GOTO :ReadVerError )
	IF "%~2" == "" ( GOTO :ReadVerError )
	FOR /F %%I IN ('readver.exe "%~2"') do set R=%%I
	
	:ReadVerError
		ENDLOCAL & SET "%~1=%R%" & EXIT /B 1
:ReadVerEnd
ENDLOCAL & SET "%~1=%R%" & EXIT /B %ERRORLEVEL%

:NewFileName
:FindNewFileName
	SHIFT
	SETLOCAL

	SET FMT=%~4
	SET "B=-"
	SET "E="
	IF /I "%FMT%" == "1" (
		SET "B=("
		SET "E=)" 
	)
	SET "baseFileName=%~2"
	SET "extName=%~3"
	SET "fileName=%baseFileName%%extName%"
	SET /A I=1
	:FindNewFileNameLoop
		IF NOT EXIST "%fileName%" GOTO :FindNewFileNameEnd
		SET /A I=%I% + 1
		SET "fileName=%baseFileName%%B%%I%%E%%extName%"
		GOTO :FindNewFileNameLoop
:FindNewFileNameEnd
ENDLOCAL & SET "%~1=%fileName%" & EXIT /B %ERRORLEVEL%