
@IF "%~1" == "" (
  @GOTO :_ShowHelp
)

@IF /I "%~1" == "-h" ( GOTO :_ShowHelp )
@IF /I "%~1" == "--help" ( GOTO :_ShowHelp )
@IF /I "%~1" == "/?" ( GOTO :_ShowHelp )

@GOTO :RemoveDir

:: ################################################
:: Functions
:_ShowHelp
	@ECHO Usage:
	@ECHO    %~n0 File1 File2 ...
	@ECHO Arguments:
	@ECHO    -h --help /?    : Show help

@EXIT /B 0

:RemoveDir
    @SETLOCAL
	:RemoveDirLoop
		@IF "%~1" == "" ENDLOCAL & EXIT /B 0 
		@IF EXIST "%~1" (
			@RMDIR /S /Q "%~1"
			@IF EXIST "%~1" (
				@ENDLOCAL & EXIT /B 1
			)
		)
	@SHIFT
	@GOTO :RemoveDirLoop
@ENDLOCAL & EXIT /B 0