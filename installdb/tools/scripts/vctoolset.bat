
SETLOCAL 
IF NOT "%~1" == "" GOTO :%~1

REM //////////////////////////////////////////////
REM Functions
:install
	@ECHO Find toolset %~2
	call findvctoolset %~2
	set "ccvar=%VC_TOOLSET_ROOT%\vcvarsall.bat"
	ENDLOCAL & call "%ccvar%" %~3 & GOTO :EOF