

REM ~ Copyright 2002-2007 Rene Rivera.
REM ~ Distributed under the Boost Software License, Version 1.0.
REM ~ (See accompanying file LICENSE_1_0.txt or http://www.boost.org/LICENSE_1_0.txt)

REM setlocal
goto Guess_Toolset

:Set_Error
color 00
goto :eof


:Clear_Error
ver >nul
goto :eof

:Test_Path
REM Tests for the given executable file presence in the directories in the PATH
REM environment variable. Additionaly sets FOUND_PATH to the path of the
REM found file.
call :Clear_Error
setlocal
set test=%~$PATH:1
endlocal
if not errorlevel 1 set FOUND_PATH=%~dp$PATH:1
goto :eof

:Test_Empty
REM Tests whether the given string is not empty
call :Clear_Error
setlocal
set test=%1
if not defined test (
    call :Clear_Error
    goto Test_Empty_End
)
set test=###%test%###
set test=%test:"###=%
set test=%test:###"=%
set test=%test:###=%
if not "" == "%test%" call :Set_Error
:Test_Empty_End
endlocal
goto :eof

:Call_If_Exists
if EXIST %1 call %*
goto :eof

:Guess_Toolset
REM Try and guess the toolset to bootstrap the build with...
REM Sets VC_TOOLSET to the first found toolset.
REM May also set VC_TOOLSET_ROOT to the
REM location of the found toolset.

call :Clear_Error
call :Test_Empty %ProgramFiles%
if not errorlevel 1 set ProgramFiles=C:\Program Files

goto GET_%1%

:GET_vc14
call :Clear_Error
if NOT "_%VS140COMNTOOLS%_" == "__" (
    set "VC_TOOLSET=vc14"
    set "VC_TOOLSET_ROOT=%VS140COMNTOOLS%..\..\VC\"
    goto :eof)	
call :Clear_Error
if EXIST "%ProgramFiles%\Microsoft Visual Studio 14.0\VC\VCVARSALL.BAT" (
    set "VC_TOOLSET=vc14"
    set "VC_TOOLSET_ROOT=%ProgramFiles%\Microsoft Visual Studio 14.0\VC\"
    goto :eof)
goto :eof

:GET_vc12
call :Clear_Error
if NOT "_%VS120COMNTOOLS%_" == "__" (
    set "VC_TOOLSET=vc12"
    set "VC_TOOLSET_ROOT=%VS120COMNTOOLS%..\..\VC\"
    goto :eof)
call :Clear_Error
if EXIST "%ProgramFiles%\Microsoft Visual Studio 12.0\VC\VCVARSALL.BAT" (
    set "VC_TOOLSET=vc12"
    set "VC_TOOLSET_ROOT=%ProgramFiles%\Microsoft Visual Studio 12.0\VC\"
    goto :eof)
goto :eof

:GET_vc11
call :Clear_Error
if NOT "_%VS110COMNTOOLS%_" == "__" (
    set "VC_TOOLSET=vc11"
    set "VC_TOOLSET_ROOT=%VS110COMNTOOLS%..\..\VC\"
    goto :eof)
call :Clear_Error
if EXIST "%ProgramFiles%\Microsoft Visual Studio 11.0\VC\VCVARSALL.BAT" (
    set "VC_TOOLSET=vc11"
    set "VC_TOOLSET_ROOT=%ProgramFiles%\Microsoft Visual Studio 11.0\VC\"
    goto :eof)
goto :eof

:GET_vc10
call :Clear_Error
if NOT "_%VS100COMNTOOLS%_" == "__" (
    set "VC_TOOLSET=vc10"
    set "VC_TOOLSET_ROOT=%VS100COMNTOOLS%..\..\VC\"
    goto :eof)
call :Clear_Error
if EXIST "%ProgramFiles%\Microsoft Visual Studio 10.0\VC\VCVARSALL.BAT" (
    set "VC_TOOLSET=vc10"
    set "VC_TOOLSET_ROOT=%ProgramFiles%\Microsoft Visual Studio 10.0\VC\"
    goto :eof)
goto :eof

:GET_vc9
call :Clear_Error
if NOT "_%VS90COMNTOOLS%_" == "__" (
    set "VC_TOOLSET=vc9"
    set "VC_TOOLSET_ROOT=%VS90COMNTOOLS%..\..\VC\"
    goto :eof)
call :Clear_Error
if EXIST "%ProgramFiles%\Microsoft Visual Studio 9.0\VC\VCVARSALL.BAT" (
    set "VC_TOOLSET=vc9"
    set "VC_TOOLSET_ROOT=%ProgramFiles%\Microsoft Visual Studio 9.0\VC\"
    goto :eof)
goto :eof

:GET_vc8
call :Clear_Error
if NOT "_%VS80COMNTOOLS%_" == "__" (
    set "VC_TOOLSET=vc8"
    set "VC_TOOLSET_ROOT=%VS80COMNTOOLS%..\..\VC\"
    goto :eof)
call :Clear_Error
if EXIST "%ProgramFiles%\Microsoft Visual Studio 8\VC\VCVARSALL.BAT" (
    set "VC_TOOLSET=vc8"
    set "VC_TOOLSET_ROOT=%ProgramFiles%\Microsoft Visual Studio 8\VC\"
    goto :eof)
goto :eof

:GET_vc7
call :Clear_Error
if NOT "_%VS71COMNTOOLS%_" == "__" (
    set "VC_TOOLSET=vc7"
    set "VC_TOOLSET_ROOT=%VS71COMNTOOLS%\..\..\VC7\"
    goto :eof)
call :Clear_Error
if NOT "_%VCINSTALLDIR%_" == "__" (
    REM %VCINSTALLDIR% is also set for VC9 (and probably VC8)
    set "VC_TOOLSET=vc7"
    set "VC_TOOLSET_ROOT=%VCINSTALLDIR%\VC7\"
    goto :eof)
call :Clear_Error
if EXIST "%ProgramFiles%\Microsoft Visual Studio .NET 2003\VC7\bin\VCVARS32.BAT" (
    set "VC_TOOLSET=vc7"
    set "VC_TOOLSET_ROOT=%ProgramFiles%\Microsoft Visual Studio .NET 2003\VC7\"
    goto :eof)
call :Clear_Error
if EXIST "%ProgramFiles%\Microsoft Visual Studio .NET\VC7\bin\VCVARS32.BAT" (
    set "VC_TOOLSET=vc7"
    set "VC_TOOLSET_ROOT=%ProgramFiles%\Microsoft Visual Studio .NET\VC7\"
    goto :eof)
goto :eof

:GET_vc6
call :Clear_Error
if NOT "_%MSVCDir%_" == "__" (
    set "VC_TOOLSET=msvc"
    set "VC_TOOLSET_ROOT=%MSVCDir%\"
    goto :eof)
call :Clear_Error
if EXIST "%ProgramFiles%\Microsoft Visual Studio\VC98\bin\VCVARS32.BAT" (
    set "VC_TOOLSET=msvc"
    set "VC_TOOLSET_ROOT=%ProgramFiles%\Microsoft Visual Studio\VC98\"
    goto :eof)
call :Clear_Error
if EXIST "%ProgramFiles%\Microsoft Visual C++\VC98\bin\VCVARS32.BAT" (
    set "VC_TOOLSET=msvc"
    set "VC_TOOLSET_ROOT=%ProgramFiles%\Microsoft Visual C++\VC98\"
    goto :eof)
call :Clear_Error
call :Test_Path cl.exe
if not errorlevel 1 (
    set "VC_TOOLSET=msvc"
    set "VC_TOOLSET_ROOT=%FOUND_PATH%..\"
    goto :eof)
call :Clear_Error
call :Test_Path vcvars32.bat
if not errorlevel 1 (
    set "VC_TOOLSET=msvc"
    call "%FOUND_PATH%VCVARS32.BAT"
    set "VC_TOOLSET_ROOT=%MSVCDir%\"
    goto :eof)
call :Clear_Error

