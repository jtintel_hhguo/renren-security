
@IF /I "%~1" == "-h" ( GOTO :_ShowHelp )
@IF /I "%~1" == "--help" ( GOTO :_ShowHelp )
@IF /I "%~1" == "/?" ( GOTO :_ShowHelp )

@GOTO :Build

:: ################################################
:: Functions
:_ShowHelp
	@ECHO Usage:
	@ECHO   SET GENTYPE    = Visual Studio 14 2015 [arch]
	@ECHO   SET SOURCEDIR  = 
	@ECHO   SET INSTALLDIR = [install]
	@ECHO   SET SLNDIR     = [build]
	@ECHO   SET ARCH       = win32 OR x64 OR others
	@ECHO   SET CONFNAME   = Debug OR Release
	@ECHO   CALL %~n0
	@ECHO Arguments:
	@ECHO    -h --help /?    : Show help

@EXIT /B 0

:Build
    SETLOCAL
	
	IF NOT "%INSTALLDIR%" == "" SET installPrefixArg=-DCMAKE_INSTALL_PREFIX=%INSTALLDIR%
	cmake -G "%GENTYPE%" -B "%SLNDIR%" "%installPrefixArg%" "%SOURCEDIR%"

	SET CONFNAME=Debug
	CALL CMakeVCMake.bat
	IF NOT "%ERRORLEVEL%" == "0" @ECHO ERROR: Build Failed & GOTO :BuildEnd
	
	SET CONFNAME=Release
	CALL CMakeVCMake.bat
	IF NOT "%ERRORLEVEL%" == "0" @ECHO ERROR: Build Failed & GOTO :BuildEnd
ENDLOCAL & EXIT /B %ERRORLEVEL%
	
:BuildEnd
	ENDLOCAL & EXIT /B %ERRORLEVEL%