
@IF /I "%~1" == "-h" ( GOTO :_ShowHelp )
@IF /I "%~1" == "--help" ( GOTO :_ShowHelp )
@IF /I "%~1" == "/?" ( GOTO :_ShowHelp )

@GOTO :Build

:: ################################################
:: Functions
:_ShowHelp
	@ECHO Usage:
	@ECHO   SET SLNDIR   = [build]
	@ECHO   SET ARCH     = win32 OR x64 OR others
	@ECHO   SET CONFNAME = Debug OR Release
	@ECHO   CALL %~n0
	@ECHO Arguments:
	@ECHO    -h --help /?    : Show help

@EXIT /B 0

:Build
    SETLOCAL
	
	IF "%ARCH%" == "" SET ARCH=win32
	IF "%CONFNAME%" == "" SET CONFNAME=Release
	
	SET "PLATFORM=%CONFNAME%|%ARCH%"
	
	FOR /F "delims=" %%I IN ('DIR /B "%SLNDIR%\*.sln"') DO (SET SLNNAME=%%I)
	
	SET SLN=%SLNDIR%\%SLNNAME%
	
	SET PRJNAME=%SLNNAME:~0,-4%
	IF NOT "%ERRORLEVEL%" == "0" @ECHO ERROR: Build Failed: %SLN% & GOTO :BuildEnd
	
	@ECHO Build "%SLN% - %PLATFORM%"
 
	devenv "%SLN%" /clean "%PLATFORM%" /project "%SLNDIR%\ALL_BUILD.vcxproj"
	devenv "%SLN%" /build "%PLATFORM%" /project "%SLNDIR%\ALL_BUILD.vcxproj"
	IF NOT "%ERRORLEVEL%" == "0" @ECHO ERROR: Build Failed: %SLN% & GOTO :BuildEnd
	
	devenv "%SLN%" /build "%PLATFORM%" /project "%SLNDIR%\RUN_TESTS.vcxproj"
	IF NOT "%ERRORLEVEL%" == "0" @ECHO ERROR: Build Failed: %SLN% & GOTO :BuildEnd
		
	devenv "%SLN%" /build "%PLATFORM%" /project "%SLNDIR%\INSTALL.vcxproj"
	IF NOT "%ERRORLEVEL%" == "0" @ECHO ERROR: Build Failed: %SLN% & GOTO :BuildEnd
	
:BuildEnd
	ENDLOCAL & EXIT /B %ERRORLEVEL%