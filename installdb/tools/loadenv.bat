
@IF "%ENVLOADED%" == "1" GOTO :EOF
@SET "ENVLOADED=1"

:: ###############################
:: Set internal environment vars
@IF EXIST "%~dp0" SET PATH=%PATH%;%~dp0
@IF EXIST "%~dp07zip" SET PATH=%PATH%;%~dp07zip
@IF EXIST "%~dp0bin" SET PATH=%PATH%;%~dp0bin
@IF EXIST "%~dp0curl" SET PATH=%PATH%;%~dp0curl
@IF EXIST "%~dp0scripts" SET PATH=%PATH%;%~dp0scripts
@IF EXIST "%~dp0sigcheck" SET PATH=%PATH%;%~dp0sigcheck
@IF EXIST "%~dp0wget" SET PATH=%PATH%;%~dp0wget
@IF EXIST "%~dp0zip" SET PATH=%PATH%;%~dp0zip
@IF EXIST "%~dp0unzip" SET PATH=%PATH%;%~dp0unzip
@IF EXIST "%~dp0xcfg" SET PATH=%PATH%;%~dp0xcfg
@IF EXIST "%~dp0cmake\bin" SET PATH=%PATH%;%~dp0cmake\bin
@IF EXIST "%~dp0nasm\nasm-win32" SET PATH=%PATH%;%~dp0nasm\nasm-win32
:: ###############################
:: Setup robocopy�� Check win version
@IF NOT EXIST "%~dp0robocopy" GOTO :UPVISTA
@VER | FINDSTR /I "5\.0\."
@IF %ERRORLEVEL% EQU 0  GOTO :WIN2000
@VER | FINDSTR /I "5\.1\."
@IF %ERRORLEVEL% EQU 0 GOTO :WINXP
@VER | FINDSTR /I "5\.2\."
@IF %ERRORLEVEL% EQU 0 GOTO :WINSERVER2003
@GOTO :UPVISTA

:WIN2000
:WINXP
:WINSERVER2003
    @SET PATH=%PATH%;%~dp0robocopy

:UPVISTA

:: Clear ERRORLEVEL
@VER >nul

:: ###############################
:: User environment vars
:: @SET PATH=%PATH%;%~dp0..\xampp\php
:: @SET PATH=%PATH%;%~dp0..\nodejs
:: @SET PATH=%PATH%;%~dp0..\composer;

@IF EXIST "%~dp0userenv.bat" CALL "%~dp0userenv.bat"