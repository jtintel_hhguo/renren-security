
:: #############################################
:: # MYSQL运维工具 - 天自动备份
:: # AUTHORS       : 郭恒辉
:: # LAST MODIFY   : 2020-08-04
:: #############################################

@ECHO OFF
@ECHO ########################################################
@ECHO 【开始备份】
SETLOCAL

CALL "%~dp0config.bat"

CALL UTILS DATE DT 2

SET PKGDIR=%~dp0dailybackup\%DT%
SET PKGFILE=%~dp0dailybackup\%DT%.7z
SET backupFile=%PKGDIR%\backup.sql

IF NOT "%ENCRYPTPWD%"==""  SET ZIPPASSWORD=%ENCRYPTPWD%

SET LOGDIR=%~dp0logs
SET LOGFILE=%LOGDIR%\daily.log

IF NOT EXIST "%LOGDIR%" MKDIR "%LOGDIR%"
IF NOT EXIST "%PKGDIR%" MKDIR "%PKGDIR%"

@ECHO %date% %time% ################################################>>"%LOGFILE%"
@ECHO %date% %time% 【备份开始】【数据库】：%DATABASE%>>"%LOGFILE%"
mysqldump --no-defaults --host=%HOST% --user=%USER% --password=%PASSWORD% --port=%PORT% -x %DATABASE% --opt --default-character-set=utf8 -e --triggers -R --hex-blob --flush-logs >"%backupFile%"

@ECHO %date% %time% 【数据压缩】 "%PKGDIR%" "%PKGFILE%" >>"%LOGFILE%"
CALL UTILS ZIP "%PKGDIR%" "%PKGFILE%"

@ECHO %date% %time% 【删除源数据】 "%PKGDIR%" >>"%LOGFILE%"
IF "%ERRORLEVEL%" == "0" DEL /S /Q "%PKGDIR%"

@ECHO %date% %time% 【备份完成】：%PKGFILE% >>"%LOGFILE%"
@ECHO %date% %time% ################################################>>"%LOGFILE%"

:END
@ECHO ********************************
@ECHO *                              *
@ECHO *                              *
IF "%ERRORLEVEL%" == "0" (
@ECHO *          备 份 成 功
@ECHO *  文件: %PKGFILE%
)
IF NOT "%ERRORLEVEL%" == "0" (
@ECHO *  备份失败！错误码：%ERRORLEVEL%
@ECHO *                              *
@ECHO *  请检查配置：config.bat
)
@ECHO *                              *
@ECHO *                              *
@ECHO ********************************
@ECHO ########################################################	
IF "%PAUSE%" == "1" PAUSE
ENDLOCAL