
:: #############################################
:: # MYSQL运维工具 - 数据安装
:: # AUTHORS       : 郭恒辉
:: # LAST MODIFY   : 2020-08-04
:: #############################################

@ECHO OFF
@ECHO ########################################################
@ECHO 【开始安装】
SETLOCAL

CALL "%~dp0config.bat"

mysql --host=%HOST% --user=%USER% --password=%PASSWORD% --port=%PORT% --comments -e "create database %DATABASE%;use %DATABASE%;set names utf8;"
IF NOT %ERRORLEVEL% EQU 0 GOTO :END

mysql --host=%HOST% --user=%USER% --password=%PASSWORD% --port=%PORT% --comments -D "%DATABASE%" --default-character-set=utf8 <"install.sql"
IF NOT %ERRORLEVEL% EQU 0 GOTO :END

IF "%APPREMOTEACCESS%" == "1" (
	mysql --host=%HOST% --user=%USER% --password=%PASSWORD% --port=%PORT% --comments -e "grant all on %DATABASE%.* to '%APPUSER%'@'%%' identified by '%APPPASSWORD%';"
	IF NOT %ERRORLEVEL% EQU 0 GOTO :END
)

mysql --host=%HOST% --user=%USER% --password=%PASSWORD% --port=%PORT% --comments -e "grant all on %DATABASE%.* to '%APPUSER%'@'localhost' identified by '%APPPASSWORD%';"
IF NOT %ERRORLEVEL% EQU 0 GOTO :END

:END
@ECHO ********************************
@ECHO *                              *
@ECHO *                              *
IF "%ERRORLEVEL%" == "0" (
@ECHO *          安 装 成 功
)
IF NOT "%ERRORLEVEL%" == "0" (
@ECHO *  安装失败！错误码：%ERRORLEVEL%
@ECHO *                              *
@ECHO *  请检查配置：config.bat
)
@ECHO *                              *
@ECHO *                              *
@ECHO ********************************
@ECHO ########################################################	
IF NOT "%NOPAUSE%" == "1" PAUSE
ENDLOCAL