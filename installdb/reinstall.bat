
:: #############################################
:: # MYSQL运维工具 - 数据重装
:: # AUTHORS       : 郭恒辉
:: # LAST MODIFY   : 2020-08-04
:: #############################################

@ECHO OFF
@ECHO ########################################################
@ECHO 【开始重装】

SET NOPAUSE=1
call uninstall.bat
IF NOT %ERRORLEVEL% EQU 0 (
	PAUSE
	GOTO :EOF
)

SET NOPAUSE=0
call install.bat